#ifndef CRYOCONTROLLERLIB_H
#define CRYOCONTROLLERLIB_H

#include <boost/tokenizer.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>
#include <stdio.h>

typedef boost::tokenizer<boost::escaped_list_separator<char>> tokenizer;
//In an ideal C++, these would be strict
typedef double Kelvin;
typedef double Millivolts;
typedef double Volts;

class Cryocontroller{
private:
  //We're using two maps over a bimap because one they never need to be resynched, hashmap for maximum speed
  std::unordered_map<Kelvin, Volts> kelvin_to_volt_map;
  std::unordered_map<Volts, Kelvin> volt_to_kelvin_map;
  
  int readCSV(std::string filepath){
    std::ifstream fs;
    fs.open(filepath, std::ios::in);
    if(fs.is_open()){
      int error = parseLines(fs);
      if(error != 0){
	std::cerr << "Failed to parse CSV" << std::endl;
	fs.close();
	exit(2);
      }
    }
    else{
      std::cerr << "File not found: " << filepath <<std::endl;
      exit(1);
    }
    fs.close();
    return 0;
  }
  
  int parseLines(std::ifstream& fs){
    std::string header, line;
    std::vector<std::string> parsed_lines;

    //Discard header
    std::getline(fs, header);

    Volts voltage;
    Kelvin degrees;
  
    //Grab all the tokens and put them in a vector
    while(std::getline(fs, line)){
      tokenizer tokens{line};
      for(const auto &token : tokens){
	parsed_lines.push_back(token);
      }

      //Grab the tokens and put them in the maps
      for(unsigned int i = 0; i < parsed_lines.size(); i+=3){
	degrees = std::stod(parsed_lines[i]);
	voltage = std::stod(parsed_lines[i+1]);

	std::pair<Volts, Kelvin> v_k_pair{voltage, degrees};
	volt_to_kelvin_map.insert({voltage, degrees});
      
	std::pair<Kelvin, Volts> k_v_pair{degrees,voltage};
	kelvin_to_volt_map.insert({degrees, voltage});
      }
    }

    //Check to see if the maps were populated at all, and if they are, hueristic check for bijectivity
    auto eq_size{volt_to_kelvin_map.size() - kelvin_to_volt_map.size()};
    if(kelvin_to_volt_map.empty() || volt_to_kelvin_map.empty() || eq_size != 0){
      return -1;
    }
    //Everything went well
    return 0; 
  }
  
public:
  Cryocontroller(std::string filepath){
    readCSV(filepath);
  };

  void printTable(){
    printf("    V");
    printf("\t\tK\n");
    for(auto& key_value_pair : volt_to_kelvin_map){
      printf("%F %10.2F \n", key_value_pair.first, key_value_pair.second);
    }
  }

  Kelvin convertVtoK(const Volts v){
    auto degrees = volt_to_kelvin_map.find(v);
    if( degrees == volt_to_kelvin_map.end()){
      fprintf(stderr, "Value %F Volts not found in table, consider implementing interpolation.\n", v);
      exit(1);
    }
    else{
      return degrees->second;
    }
  } 
  
  Kelvin convertmVtoK(const Millivolts mv){    
    Volts voltage = mv*1E-3;
    return convertVtoK(voltage);
  }
  
  Volts convertKtoV(const Kelvin deg){
    auto voltage = kelvin_to_volt_map.find(deg);  
    if( voltage == kelvin_to_volt_map.end()){
      std::cerr << "Value " << deg << " Kelvin not found in table, consider implementing interpolation. " << std::endl;
      exit(1);
    }
    else{
      return voltage->second;
    }    
  }
  
  Millivolts convertKtomV(const Kelvin deg){
    return convertKtoV(deg)*1E-3;
  }
    
  
  ~Cryocontroller() = default;
  
};

#endif
