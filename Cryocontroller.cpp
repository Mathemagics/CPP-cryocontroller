#include "CryocontrollerLib.hpp"
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

void usage();

int main (int argc, char* argv[]){
  int ch, calc_voltage_flag = 0, calc_degrees_flag = 0;
  int millivolt_in_flag = 0, volt_in_flag = 0, millivolt_out_flag = 0;
  int volt_out_flag = 0, print_flag = 0;
  
  double voltage, degrees = 0;
  std::string csv_path = "";

  /* options descriptor */
  static struct option longopts[] = {
    { "volt_input", required_argument, NULL, 'v' },
    { "output_voltage_units", required_argument, NULL, 'o'},
    { "input_voltage_units", required_argument, NULL, 'i'},
    { "csv_file", required_argument, NULL, 'f'},
    { "degree_input", required_argument, NULL, 'd'},
    { "print table", no_argument, &print_flag, 1},
    { "help", no_argument, NULL, 'h'},
    { NULL, 0, NULL, 0}
  };

  while ((ch = getopt_long(argc, argv, "v:o:d:f:i:ph", longopts, NULL)) != -1){
    switch(ch){
    case 'd':
      //A kelvin unit was given, prepare to find corresponding voltage.
      calc_voltage_flag = 1;
      degrees = std::stod(optarg);
      break;
    case 'v':
      //A voltage unit was given, prepare to find corresponding kelvin degrees.
      calc_degrees_flag = 1;
      voltage = std::stod(optarg);
      break;
    case 'o':
      //A voltage output unit was supplied, prepare to handle output units.
      if( (strcmp(optarg, "mV") == 0) ){
	millivolt_out_flag = 1;
	volt_out_flag = 0;
      }
      else if( (strcmp(optarg,"V") == 0) ){
	volt_out_flag = 1;
	millivolt_out_flag = 0;
      }
      else{
	std::cerr << "Unsupported unit selected: " << optarg << std::endl;
	exit(1);
      }
      break;
    case 'i':
      //A voltage input unit was supplied, prepare to handle input units.
      if( (strcmp(optarg, "mV") == 0) ){
	millivolt_in_flag = 1;
	volt_in_flag = 0;
      }
      else if( (strcmp(optarg,"V") == 0) ){
	volt_in_flag = 1;
	millivolt_in_flag = 0;
      }
      else{
	std::cerr << "Unsupported unit selected: " << optarg << std::endl;
	exit(1);
      }
      break;
    case 'p':
      print_flag = 1;
      break;
    case 'f':
      csv_path = optarg;
      break;
    case 'h':
      usage();
      exit(0);
      break;
    case '?':
      std::cerr << "Invalid Options." <<std::endl;
      exit(1);
      break;
    } 
  }

  if(!csv_path.empty()){
    double res;
    Cryocontroller c_controller = Cryocontroller(csv_path);
    //Handle quicks
    if(print_flag){
      c_controller.printTable();
      exit(0);
    }

    //Handle desire for voltages
    else if(calc_degrees_flag){
      if(millivolt_in_flag){
	Millivolts volt_in{voltage};
	res = c_controller.convertmVtoK(volt_in);
	if(volt_out_flag){
	  res*=1E3;
	}
      }
      else if(volt_in_flag){
	Volts volt_in{voltage};
        res = c_controller.convertVtoK(volt_in);
	if(millivolt_out_flag){
	  res*=1E-3;
	}
      }
      else{
	std::cerr << "Input voltage units not provided";
	exit(1);
      }
      std::cout << res << std::endl;
    }


    //Handle desire for degrees
    else if(calc_voltage_flag){
      if(millivolt_out_flag){
        res = c_controller.convertKtomV(degrees);       
      }
      else if(volt_out_flag){
        res = c_controller.convertKtoV(degrees);	
      }
      else{
	std::cerr << "No output units selected." << std::endl;
	exit(1);
      }
      std::cout << res << std::endl;
    }
  }

  //Quick exit
  else{
    std::cerr << "Error: No csv file supplied." << std::endl;
    exit(1);
  }
  return 0;
}


void usage(){
  std::cout << "Usage: Cryocontroller (--csv_file <file>) (-d <degrees Kelvin> (-o (<mV> | <V>))  |  v (-i (<mV> | <V>))  ) " << std::endl;
  std::cout << " -h, --help \t " << "Print usage" <<std::endl;
  
  std::cout << " -f, --csv_file \t " << "Provide a calibration table in CSV format where the first column is voltage and the second is degrees." <<std::endl;
  std::cout << " -v, --volt_input \t " << "Provide a voltage input, you must also provide a --input_voltage_units option. You will receive Kelvin units." <<std::endl;
  std::cout << " -d, --degree_input \t " << "Provide a degree input in Kelvins. You will receive a voltage. You must also provide a --output_voltage_units option." <<std::endl;
  std::cout << " -i, --input_voltage_units \t " << "Voltage Input Units: \"mV\" or \"V\" are supported." <<std::endl;
  std::cout << " -o, --output_voltage_units \t " << "Voltage Output  Units: \"mV\" or \"V\" are supported." <<std::endl;

  std::cout << "\nExamples: " << std::endl;
  std::cout << "Cryocontroller -f DT-670.csv -d 65 -o V \tLoads the calibration table DT-670, finds the corresponding voltage in Volts when 65 degrees Kelvin is the desired temperature." << std::endl;
  std::cout << "Cryocontroller -f DT-670.csv -d 65 -o mV \tLoads the calibration table DT-670, finds the corresponding voltage in MilliVolts when 65 degrees Kelvin is the desired temperature." << std::endl;
  std::cout << "Cryocontroller -f DT-670.csv -v 1.64429 -o V \tLoads the calibration table DT-670, finds the corresponding temperature in Kelvins when 1.0731 Volts is supplied." << std::endl;
  std::cout << "Cryocontroller -f DT-670.csv -mv 826.560 -o mV \tLoads the calibration table DT-670, finds the corresponding temperature in Kelvins when 1073.1 MilliVolts is supplied." << std::endl;
}
  

