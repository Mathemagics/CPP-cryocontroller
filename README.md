# Purpose
This is a toy project.
It contains a header library that parses a CSV file of calibration data for a cryogenic cooler sensor, a Lakeshore DT-670. It creates two maps which form a bijective map between voltages and temperatures. The Cryocontroller executable is basically a toy program for demonstrating the functions in the library. Interpolation is not implemented. If you're a math student, you'll want to look into interpolating polynomials, I think the data is actually piecewise linear, but I haven't looked at it.

https://www.lakeshore.com/products/cryogenic-temperature-sensors/silicon-diodes/dt-670/pages/Specifications.aspx

# Dependencies 
- POSIX
- Boost (For tokenizer library)

# Compiling
g++ -o Cryocontroller Cryocontroller.cpp  CryocontrollerLib.hpp

# Usage
Cryocontroller --help

# License
 Openbsd License

# Todo
- Data interpolation

# News
- 2019-APR-07 This project is abandoned
